import os
import logging.handlers

LOG_FOLDERS_PATH = os.path.dirname(os.path.abspath(__file__))
LOG_FILE_PATH = os.path.join(LOG_FOLDERS_PATH, 'funderfill.log')
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
# Логгер с именем funderfill
logger = logging.getLogger('funderfill')
# Создаем обработчик с ротацией файла лога по дням
handler = logging.handlers.TimedRotatingFileHandler(LOG_FILE_PATH, when='D')
# Связываем логгер с обработчиком
logger.addHandler(handler)
# Связывваем обработчки с форматером
handler.setFormatter(formatter)
# Задем уровень логирования
logger.setLevel(logging.INFO)
