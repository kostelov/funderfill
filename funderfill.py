# from __future__ import print_function
import pickle
import os.path
import base64
import logging
from apiclient import errors
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import log_config

# При изменении этих областей удалите файл token
SCOPES = ['https://www.googleapis.com/auth/gmail.modify']
logger = logging.getLogger('funderfill')


def auth():
    creds = None
    # Файл token хранит маркеры доступа и обновления пользователя, создается
    # автоматически при первом завершении потока авторизации.
    if os.path.exists('token'):
        with open('token', 'rb') as token:
            creds = pickle.load(token)
    # Если нет доступных (действительных) учетных данных, дайте пользователю
    # войти в систему.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'client_secret.json', SCOPES)
            creds = flow.run_local_server()
        # Сохраняем учетные данные для следующего запуска
        with open('token', 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)

    # добавляем информацию в лог файл
    if service:
        logger.info('Авторизация успешна')
    else:
        logger.warning('Сбой при попытки авторизации')

    return service


def get_labels(service):
    # Получаем список меток в почтовом ящике
    response = service.users().labels().list(userId='me').execute()
    labels = response.get('labels', [])

    if not labels:
        print('Меток не найдено')
    else:
        print('Метки:')
        for label in labels:
            print(label['name'])


def get_attachments(service):
    try:
        # Отфильтруем сообщения по теме и добавим в список
        response = service.users().messages().list(userId='me', q='subject:torrent').execute()
        messages = []
        if 'messages' in response:
            messages.extend(response['messages'])

        # добавляем информацию в лог файл
        logger.info('Новых сообщений {}'.format(len(messages)))

        for message in messages:
            # Перебираем полученные сообщения и извлекаем вложение, если таковое есть
            msg = service.users().messages().get(userId='me', id=message['id']).execute()
            for part in msg['payload']['parts']:
                if part['filename']:
                    if 'data' in part['body']:
                        data = part['body']['data']
                    else:
                        atach_id = part['body']['attachmentId']
                        atach = service.users().messages().attachments().get(userId='me', messageId=msg['id'],
                                                                             id=atach_id).execute()
                        data = atach['data']
                    file_data = base64.urlsafe_b64decode(data.encode('UTF-8'))
                    # Сохраняем вложение на диск
                    with open(part['filename'], 'wb') as f:
                        f.write(file_data)

                        # добавляем информацию в лог файл
                        logger.info('Файл {} получен'.format(part['filename']))

            # Удаляем письма с вложениями в корзину
            service.users().messages().trash(userId='me', id=msg['id']).execute()
            logger.info('Сообщение {} помещено в корзину'.format(msg['id']))
    except errors.HttpError as error:
        logger.error('Произошла ошибка: {}'.format(error))


if __name__ == '__main__':
    gmail_service = auth()
    get_attachments(gmail_service)
