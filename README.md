# Gmail Python

Выполните шаги, описанные в [Gmail Python Quickstart](
https://developers.google.com/gmail/api/quickstart/python), и примерно через
пять минут у вас будет простое приложение командной строки Python, которое
отправляет запросы в API Gmail.

## Установка

```
pip install -r requirements.txt
```

## Запуск

```
python my_programm.py
```

## [Области Gmail API](https://developers.google.com/gmail/api/auth/scopes)

 - **https://www.googleapis.com/auth/gmail.readonly**
   Чтение всех ресурсов и их метаданных - без операций записи.
   
 - **https://www.googleapis.com/auth/gmail.compose**
   Создание, чтение, изменение и  удаление черновиков. Отправка собщений и
   черновиков.
   
 - **https://www.googleapis.com/auth/gmail.send**
   Только отправка сообщений. Без возможности чтения или изменения.
   
 - **https://www.googleapis.com/auth/gmail.insert**
   Только вставка и импорт сообщений.
   
 - **https://www.googleapis.com/auth/gmail.labels**
   Создание, чтение, изменение и удаление меток.
   
 - **https://www.googleapis.com/auth/gmail.modify**
   Все операции чтения/записи, кроме немедленного удаления потоков и сообщений,
   минуя корзину. Позволяет работать с вложениями.
   
 - **https://www.googleapis.com/auth/gmail.metadata**
   Чтение метаданных ресурсов, включая метки, записи истории и заголовки
   сообщений электронной почты, но не тело сообщения или вложения.
   
 - **https://www.googleapis.com/auth/gmail.settings.basic**
   Управление основными настройками почты.
   
 - **https://mail.google.com/**
   Полный доступ к аккаунту, включая постоянное удаление тем и сообщений. Эту
   область следует запрашивать только в том случае, если вашему приложению
   необходимо немедленно и навсегда удалить потоки и сообщения, минуя корзину;
   все другие действия могут быть выполнены с менее разрешающими областями.
